export default () => {
  const button = document.querySelector(".header__burger");
  const menu = document.querySelector(".header__mobile-menu");
  const activeClass = "header__burger--active";
  const menuClasses = 'header__mobile-menu--active'

  button.addEventListener("click", () => {
    button.classList.toggle(activeClass);
    menu.classList.toggle(menuClasses);
  });

  document.addEventListener("scroll", () => {
    button.classList.remove(activeClass);
    menu.classList.remove(menuClasses);
  })
}