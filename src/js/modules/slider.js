import "owl.carousel";

export default () => {
  $('.plans__list').owlCarousel({
    loop: false,
    margin: 0,
    nav: true,
    responsive:{
      0: {
        items: 1,
        nav: false,
      },
      768: {
        items: 2,
        nav: false,
        stagePadding: 30,
      },
      1110: {
        items: 3,
        mouseDrag: false,
      }
    }
  })
}